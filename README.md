<a href="http://c4ad.eu" target="_blank"><img src="img/C4AD-letterHead.svg.png"/></a>

# Sound trigger mechanism   
 
## Requirements

A fout setting spin switch combined with three normally off switches.This will give a combination of 5 different sounds: back, front, left, right and laterals. 

Sounds should be loaded via an SD card or easy to drag and drop audio. Technology such as arduino is acceptable. 

Sound quality not critical. 

## Existing  technology

SD card, 4 way switch, push switch, arduino, arduino shield, 

* [very DIY solution](https://arduino.stackexchange.com/questions/12735/is-there-a-way-to-play-sounds-from-an-arduino-without-using-a-sound-shield)
* [https://maxoffsky.com/maxoffsky-blog/how-to-play-wav-audio-files-with-arduino-uno-and-microsd-card/](https://maxoffsky.com/maxoffsky-blog/how-to-play-wav-audio-files-with-arduino-uno-and-microsd-card/)
* [https://www.arduino.cc/en/Tutorial/SimpleAudioPlayer](https://www.arduino.cc/en/Tutorial/SimpleAudioPlayer)
* [http://www.instructables.com/id/Audio-Playback-From-SD-Card-With-Arduino/](http://www.instructables.com/id/Audio-Playback-From-SD-Card-With-Arduino/)
* [https://circuitdigest.com/microcontroller-projects/arduino-audio-music-player](https://circuitdigest.com/microcontroller-projects/arduino-audio-music-player)
* [adafruit music and sound](https://www.adafruit.com/product/175)
* [adafruit music maker](https://www.adafruit.com/product/1788)
* [ada fruit Music Maker](https://www.adafruit.com/product/3357)
* [ada fruit Feather](https://learn.adafruit.com/mp3-feather-gordon-cole)
* [ada fruit chirp owl gemma](https://www.adafruit.com/product/1759)
* [ada fruit sound maker](https://learn.adafruit.com/adafruit-wave-shield-audio-shield-for-arduino)
* [cool components sparkfun and lil-pad](https://coolcomponents.co.uk/search?type=article%2Cpage%2Cproduct&q=sound*+player*)
* [cool Components basic pack](https://coolcomponents.co.uk/products/audio-sound-breakout-wtv020sd)
* [rotary switch](https://uk.rs-online.com/web/c/switches/rotary-switches-accessories/rotary-switches/?searchTerm=4%20way%20switch)
* [NO-momentary switch](https://uk.rs-online.com/web/c/switches/push-button-switches-accessories/push-button-switches/?searchTerm=button)
* [Raspberry Pi sound board](https://makezine.com/projects/make-33/simple-soundboard/)
* [Raspberry Pi sound board adafruit](https://cdn-learn.adafruit.com/downloads/pdf/playing-sounds-and-using-buttons-with-raspberry-pi.pdf)
* [cool components](https://coolcomponents.co.uk/products/arduino-mkr-zero) 

## Proposal and time scales

* Delivery time 2 weeks.
* 1 day work to get sound out of speakers. 
* 2 hrs research for best solution available in UK, and circuit diagram for 4 sounds
* <£100 in materials.
* Half a day to get different sounds working
 

## Option chosen

* [adafruit](https://www.adafruit.com/product/987)
* [adafruit tutorial](https://learn.adafruit.com/adafruit-audio-fx-sound-board/pinouts)

### BOM

* Adafruit Stereo 3.7W Class D Audio Amplifier - MAX98306 (Mono would have been enough)
* Adafruit Speaker - 3" Diameter - 4 Ohm 3 Watt (Smaller speaker can be uses e.g. 16 Ohm) 
* Adafruit 3 x AAA Battery Holder with On/Off Switch and 2-Pin JST (or use a 5V mobile power supply).
* Adafruit Tactile Button switch (6mm) x 20 pack (or any other normally off switch).
* Adafruit Mini 8-Way Rotary Selector Switch - SP8T (we will only use 4 of the ways,the rest will be soldered together
* Adafruit Audio FX Sound Board - WAV/OGG Trigger with 16MB Flash (Smaller flash memory is cheaper. no need for so much)
* Breadboard
* Wires for breadboard use.


### Sounds

We don't have a sound assignment for ON and OFF. 

When you see NEXT it means it will do the sound labelled NEXT0 first and NEXT1 the next time the button is pressed. The tool positioning sound will go first followed by switch position sound. 


* ToolLeft --> T05
* ToolRight --> T04
* ToolMiddle --> T00NEXT0, TO1NEXT0, TO2NEXT0, TO3NEXT0
* SwitchFront-->T00NEXT1
* SwitchSide -->T01NEXT1, T03NEXT1
* SwitchRear -->T02NEXT1
* 

Should sound need to be amplified the Jumper can be set at the adequate volume. 
 

### Sound creation

Using laptop microphone and the following command in Ubuntu terminal:
$ arecord rear.wav

Recording WAVE 'rear.wav' : Unsigned 8 bit, Rate 8000 Hz, Mono
Ctrl+C stops the recording

OGG and WAV files are accepted. 

When changing sounds ensure that files are fully deleted. Check for hidden folders. 

OGG files are compressed and will take up less space. 


## Connection diagram
<a href="https://learn.adafruit.com/adafruit-audio-fx-sound-board/overview" target="_blank"><img src="img/WireDiagram.png"/></a>

<a href="http://c4ad.eu" target="_blank"><img src="img/SolderedFinish_small.jpg"/></a>


<video src="img/Demo.ogv"  width="200">
<a href="img/Demo.ogv">Download Video Demo</a>
</video>

# Licence

<a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank"><img src="img/cc-by-sa.png"/></a>
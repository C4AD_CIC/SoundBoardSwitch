<a href="http://c4ad.eu" target="_blank"><img src="/home/amunizp/Templates/img/C4AD-letterHead.svg.png"/></a>

# Original Licence for content

[Lady ada](https://learn.adafruit.com/users/adafruit2) Attribution-ShareAlike Creative Commons  3.0
2018-06-21
Modifications by C4AD. Same licence. 

 
## Sub Title



<a href="https://creativecommons.org/licenses/by-sa/4.0/" target="_blank"><img src="/home/amunizp/Templates/img/cc-by-sa.png"/></a>